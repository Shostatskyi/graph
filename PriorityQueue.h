#pragma once
#include <iostream>

class PriorityQueue {
	int    _maxSize;
	int    _size;
	struct mystr {
		int key;
		int val;
	} *_heap;

public:
	PriorityQueue(int maxSize) : _maxSize(maxSize), _size(0), _heap(new mystr[maxSize]) { }

	int parent(int i) { return i / 2; }
	int leftChild(int i) { return i * 2; }
	int rightChild(int i) { return i * 2 + 1; }
	int size() { return _size; }

	void siftUp(int i);
	void siftDown(int i);
	void insert(int val, int key);
	int extractMax();
	void remove(int i);
	void chagePriority(int i, int p);
	void show();
	static void swap(mystr &a, mystr &b);
};

