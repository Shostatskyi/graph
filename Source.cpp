#pragma once
#include <iostream>
#include "Graph.h"
#include "PriorityQueue.h"

using namespace std;

void main()
{   
	int graph[9][9] = { 
	{ 0, 4, 0, 0, 0, 0, 0, 8, 0 },
	{ 4, 0, 8, 0, 0, 0, 0, 11, 0 },
	{ 0, 8, 0, 7, 0, 4, 0, 0, 2 },
	{ 0, 0, 7, 0, 9, 14, 0, 0, 0 },
	{ 0, 0, 0, 9, 0, 10, 0, 0, 0 },
	{ 0, 0, 4, 14, 10, 0, 2, 0, 0 },
	{ 0, 0, 0, 0, 0, 2, 0, 1, 6 },
	{ 8, 11, 0, 0, 0, 0, 1, 0, 7 },
	{ 0, 0, 2, 0, 0, 0, 6, 7, 0 }
	};

	Graph G();

	Graph G1(9, *graph);
	G1.dijkstra(0);

	Graph G2(G1);
	G2.dijkstra(0);


	system("pause");
}