#include"PriorityQueue.h"

void PriorityQueue::swap(mystr &a, mystr &b)
{
	mystr tmp = a;
	a = b, b = tmp;
}

void PriorityQueue::siftUp(int i)
{
	while (i > 1 && _heap[this->parent(i)].val < _heap[i].val)
	{
		swap(_heap[this->parent(i)], _heap[i]);
		i = this->parent(i);
	}
}

void PriorityQueue::siftDown(int i)
{
	int maxIndex = i, l, r;
	l = this->leftChild(i);
	if (l <= _size && _heap[l].val > _heap[maxIndex].val)
		maxIndex = l;

	r = this->rightChild(i);
	if (r <= _size && _heap[r].val > _heap[maxIndex].val)
		maxIndex = r;

	if (i != maxIndex) {
		swap(_heap[i], _heap[maxIndex]);
		this->siftDown(i + 1);
	}
}

void PriorityQueue::insert(int val, int key)
{
	if (_size != _maxSize)
	{
		_size++;
		_heap[_size].val = val;
		_heap[_size].key = key;
		this->siftUp(_size);
	}
}

int PriorityQueue::extractMax()
{
	int result = _heap[1].key;
	_heap[1] = _heap[_size];
	this->siftDown(1);
	_size--;
	return result;
}

void PriorityQueue::remove(int i)
{
	_heap[i].val = INT_MAX;
	this->siftUp(i);
	this->extractMax();

}

void PriorityQueue::chagePriority(int i, int p)
{
	int old = _heap[i].val;
	_heap[i].val = p;
	if (p > old)
		this->siftUp(i);
	else
		this->siftDown(i);
}

void PriorityQueue::show()
{
	for (int i = 1; i <= _size; i++)
	{
		std::cout << _heap[i].val << " \n";
	}
	std::cout << "\n";
}
