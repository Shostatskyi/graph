#include "Graph.h"
#include "PriorityQueue.h" // My customized priority queue for keep distances and extracting min of them
#include <iostream>

using namespace std;
 
Graph::Graph(Graph const &graph)
{
	_V = graph._V;
	_G = new int*[_V];
	
	for (int i = 0, j; i < _V; i++)
	{
		_G[i] = new int[_V];

		for (j = 0; j < _V; j++) 
		{
			_G[i][j] = graph._G[i][j];
		}
			
	}
}

Graph::Graph(int vertices, int *graph)
{
	_V = vertices;
	_G = new int*[_V];

	for (int i = 0, j; i < _V; i++)
	{
		_G[i] = new int[_V];

		for (j = 0; j < _V; j++, graph++)
		{
			_G[i][j] = *graph;
		}
	}
}

Graph::~Graph()
{
	for (int i = 0, j; i < _V; i++)
			delete [] _G[i];
	delete[] _G;
}

int* Graph::dijkstra(int s)
{
	int *dist = new int[_V], *ptr1 = dist, i, v, u;

	for (i = 0; i < _V; i++)
		dist[i] = INT_MAX;

	 dist[s] = 0;

	// Extracting min from an array takes longer than from binary heap.
	// That's why I created and use below own priority queue for this purpose.
	
	PriorityQueue heap(_V);
	heap.insert(dist[s]*-1, s); // I multiply by -1 so that the minimum value is always at the top of my priority queue  

	while (heap.size()) {
		for (u = heap.extractMax(), v = 0; v < _V; v++) { // all (u, v) � E, i.e. look for distances to all adjacent vertices of the current vertice
			if (dist[v] > dist[u] + _G[u][v] && _G[u][v]) {
				dist[v] = dist[u] + _G[u][v];
				heap.insert(dist[v] * -1, v); // push all possibly minimum distances into the heap
			}
		}
	}

	// Cout - just to test the method
	for (int i = 0; i < _V; i++) 
		cout << i << ' ' << dist[i] << endl;
	cout << endl; 

	return dist;
}