#pragma once
#include <iostream>
using namespace std;

class Graph
{
	int **_G;
	int   _V;

	// will also make as adjacency list  
	// what  does list< pair<int, int> > *adj;   mean?
	// Can we use sets (disjoint sets) here? 
	// Should such class contain methods for minimum spanning tree?

	public: 
		Graph() : _V(0), _G(NULL) {}
		Graph(Graph const &graph);
		Graph(int vertices, int *graph);
		~Graph();
		 
		int* dijkstra(int S);

		// will be other methods  - bidirectional Dijkstra can be 1000s of times faster than Dijkstra for social networks 		// and just 2x speedup for road networks
};